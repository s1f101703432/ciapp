package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "pAssword");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException, UserNotFoundException  {
        User user = loginManager.login("Testuser1", "pAssword");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("pAssword"));
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginWrongPassword() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("Testuser1", "1234");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("Iniad", "password");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLoginUnregisteredUser2() throws LoginFailedException, UserNotFoundException {
        User user = loginManager.login("testuser1", "password");
    }
    
}